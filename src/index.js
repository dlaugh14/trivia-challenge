import React from 'react';
import ReactDOM from 'react-dom';
import Questions from './actions/containers/questions';
import App from './actions/containers/addAnswer';
import Results from './actions/containers/results';
import registerServiceWorker from './registerServiceWorker';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { compose, applyMiddleware, createStore } from 'redux';

import {persistStore, persistReducer} from 'redux-persist';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'
import './index.css';

import {PersistGate} from 'redux-persist/lib/integration/react';
import storage from 'redux-persist/lib/storage';


import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import allReducers from './reducers';


const logger = createLogger();

const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, allReducers);
const enhancer = compose(applyMiddleware(thunk, logger));
let store = createStore(persistedReducer, {}, enhancer);
let persistor = persistStore(store);
const Root = ({ store }) => (
<Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
        <Router>
            <div>
                <Route exact path="/" component={App}/>
                <Route exact path="/questions" component={Questions}/>
                <Route exact path="/results" component={Results}/>
            </div>
        </Router>
    </PersistGate>
</Provider>
);

Root.propTypes = { store: PropTypes.object.isRequired }

ReactDOM.render(
    <Root store={store} />,
    document.getElementById('root')
)
registerServiceWorker()
