import React, { Component } from 'react';
import './App.css';
import Home from './components/controllers/core/main/home';
import 'semantic-ui-css/semantic.min.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
    <div>
      <Home {...this.props}/>
    </div>
    );
  }
}

export default App;
