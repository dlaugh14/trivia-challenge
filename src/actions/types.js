/**
 * Created by laugh on 7/24/2018.
 */
// types.js
const SET_QUESTIONS = 'SET_QUESTIONS'
const ADD_ANSWER = 'ADD_ANSWER'
const SET_QUESTION_COUNT = 'SET_QUESTION_COUNT'

export default {
  SET_QUESTIONS,
  ADD_ANSWER,
  SET_QUESTION_COUNT
}