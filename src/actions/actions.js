/**
 * Created by laugh on 7/25/2018.
 */
// actions.js
import types from './types.js'

export function setQuestions (values) {
  return function (dispatch) {
    dispatch({ type: types.SET_QUESTIONS, questions: values })
  }
}

export function addAnswer(results) {
  return function (dispatch) {
    dispatch({ type: types.ADD_ANSWER, answers: results })
  }
}

export function setQuestionCount(results) {
  return function (dispatch) {
    dispatch({ type: types.SET_QUESTION_COUNT, questions: results })
  }
}

export default {
  setQuestions,
  setQuestionCount,
  addAnswer
}