/**
 * Created by laugh on 7/25/2018.
 */

import { connect } from 'react-redux';
import { addAnswer, setQuestions, setQuestionCount } from '../actions';
import Questions from '../../components/controllers/core/main/questions';
const mapDispatchToProps = {
  addAnswer,
  setQuestions,
  setQuestionCount
};
const mapStateToProps = state => {
    return {
        answers: state.answers,
        results: state.results,
        questionCount: state.questionCount
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Questions);