/**
 * Created by laugh on 7/14/2018.
 */
import React, { Component } from 'react';
import { Container } from 'semantic-ui-react'
import Title from '../../../views/title';
import Description from '../../../views/description';
import Button from '../../../views/button';
import Navigation from '../../core/header/navigation';

import {
    Link
} from 'react-router-dom'
const axios = require('axios');
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const globalProps = this.props;
        // Make a request for a user with a given ID
        axios.get('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
            .then(function (response) {
                // handle success
                if (globalProps.answers && globalProps.answers && globalProps.answers === 10) {
                    globalProps.addAnswer([]);
                    globalProps.setQuestionCount(0);
                }
              globalProps.setQuestions(response.data.results);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }

    render() {
        return (
            <Container>
                <Navigation page="home"/>
                <Title/>
                <br/><br/><br/>
                <Description message="You will be presented with 10 True or False questions." size="h2" alignment="center"/>
                <br/><br/>
                <Description message="Can you score 100%?" size="h3" alignment="center"/>
                <br/><br/>
                <Link to="/questions"><Button inverted="inverted" color="teal" text="Begin"/></Link>
            </Container>
        );
    }
}
// move all actions to below and out of components
// const mapDispatchToProps = {
//     setQuestions,
// };
//
// // const mapDispatchToProps = (dispatch) => {
// //     debugger;
// //     return bindActionCreators({setQuestions: setQuestions}, dispatch);
// //
// // };
//
// function setQuestion(results) {
//     return { type: "SET_QUESTIONS", questions: results}
// }
//
// function setQuestionCount(results) {
//     return { type: "SET_QUESTION_COUNT", questions: results}
// }
//
// const mapStateToProps = state => {
//     return {
//         session: state,
//     }
// };

export default Home;
