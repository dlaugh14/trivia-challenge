/**
 * Created by laugh on 7/15/2018.
 */
import React, { Component } from 'react';
import { Container, Segment } from 'semantic-ui-react'
import Description from '../../../views/description';
import Button from '../../../views/button';
import ListSection from '../../../views/list';
import Navigation from '../../core/header/navigation';

import {
    Redirect
} from 'react-router-dom';
class Results extends Component {
    constructor(props) {
        super(props);
        this.state = {"question": "", "redirect": "", "questionNumber": 0};

        this.clearStorage = this.clearStorage.bind(this);
    }

    createMarkup(html) {
        return {__html: html};
    }
    componentDidMount() {

    }

    clearStorage() {
        this.props.addAnswer([]);
        this.props.setQuestionCount(0);
        this.setState(prevState => ({
            redirect: <Redirect to="/"/>
        }));
    }


    render() {
        let output = "";
        if (this.props.results && this.state.redirect === "") {
            const answers = this.props.answers.answers;
            const triviaQuestions = this.props.results.questions;
            localStorage.setItem('questionFix', JSON.stringify(triviaQuestions));
            let correctAnswerCount = 0;
            let cleanQuestions = JSON.parse(localStorage.getItem('questionFix'));
            cleanQuestions.map((item, index) => (
                cleanQuestions[index].question = <p dangerouslySetInnerHTML={{__html: item.question}}></p>
            ));

            answers.map((item, index) => {
                if (answers[index].result === 'correct') {
                  return correctAnswerCount++;
                } else {
                    return false;
                }

            });
            output = <div>
                <Navigation page="results"/>
                <Container text>
                    <Description message={"You Scored"} size="h2" alignment="center"/>
                    <Description message={correctAnswerCount + " / " + cleanQuestions.length} size="h2" alignment="center"/>
                    <Segment.Group>
                        <Segment><ListSection questions={cleanQuestions} answers={answers}></ListSection></Segment>
                    </Segment.Group>
                    <br/>
                    <Button inverted="inverted" function={this.clearStorage} color="teal" text="Play Again?"/>
                </Container>
            </div>;
        }
        if (this.state.redirect !== "") {
            output = this.state.redirect;
        }
        return (
            <div>
                {output}
            </div>
        );
    }
}

export default Results
