/**
 * Created by laugh on 7/15/2018.
 */
import React, { Component } from 'react';
import { Container } from 'semantic-ui-react'
import Description from '../../../views/description';
import QuestionSection from '../../../views/question-section';
import Button from '../../../views/button';
import Navigation from '../../core/header/navigation';

import {
    Redirect
} from 'react-router-dom';
class Questions extends Component {
    constructor(props) {
        super(props);
        this.state = {"question": "", "redirect": "", "questionNumber": 0, 'questionCount': 0, 'currentQuestion': ''};

        this.createMarkup = this.createMarkup.bind(this);
        this.updateQuestionCount = this.updateQuestionCount.bind(this);
        this.updateView = this.updateView.bind(this);
    }

    createMarkup(html) {
    return {__html: html};
    }
    componentDidMount() {
        this.updateView();
    }

    updateView(count = 0) {
      if (this.props.results && this.state.redirect === "") {
        const triviaQuestions = this.props.results.questions;
        let questionCount = count;
        if (count === 0) {
          questionCount = this.props.questionCount.questions;
        }
        this.setState(prevState => ({
          questionCount: questionCount
        }));
        this.setState(prevState => ({
          questionNumber: questionCount
        }));
        if (triviaQuestions.length > 0) {
          this.setState(prevState => ({
            currentQuestion: triviaQuestions[questionCount]
          }));
          this.setState(prevState => ({
            question: triviaQuestions[questionCount]
          }));
        }
      }
    }

    updateQuestionCount(e) {
        let answer = "";
        let result = "";
        if (e.target.value) {
                answer = e.target.value;
        }
        if (answer === this.state.question.correct_answer) {
            result = "correct";
        } else {
            result = "incorrect";
        }
        let answers = this.props.answers;
        let userAnswer = "";
        if (answers.length === 0) {
            userAnswer= [{"answer": answer, "result": result}];
        } else {
            answers = this.props.answers.answers;
            answers.push({"answer": answer, "result": result});
            userAnswer = answers;
        }

        this.props.addAnswer(userAnswer);
        this.props.setQuestionCount(this.props.questionCount.questions + 1);
        if (this.state.questionNumber + 1 === this.props.results.questions.length ) {
            this.setState(prevState => ({
                redirect: <Redirect to="/results"/>
            }));
        }
      this.updateView(this.props.questionCount.questions + 1);
    }

    render() {
        let output = "";
        if (this.props.results && this.state.redirect === "") {

            let questionHtml = <p dangerouslySetInnerHTML={{__html: this.state.currentQuestion.question}}></p>

            output = <div>
                <Container>
                    <Navigation page="questions"/>
                    <br/><br/>
                    <Description message={this.state.currentQuestion.category} size="h2" alignment="center"/>
                    <br/><br/>
                    <QuestionSection question={questionHtml}/>
                    <br/>
                    <Description message={this.state.questionCount + 1 + " of " + this.props.results.questions.length} size="h4" alignment="center"/>
                    <br/>
                    <Button inverted="inverted" function={this.updateQuestionCount} color="teal" text="True"/>
                    <Button inverted="inverted" function={this.updateQuestionCount} color="red" text="False"/>
                </Container>
            </div>;
        }
        if (this.state.redirect !== "") {
            output = this.state.redirect;
        }
        return (
            <div>
                {output}
            </div>
        );
    }
}

export default Questions
