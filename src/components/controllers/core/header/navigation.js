/**
 * Created by laugh on 7/14/2018.
 */
import React, { Component } from 'react';
import Breadcrumbs from '../../../views/breadcrumbs';
import { Container, Grid, GridRow } from 'semantic-ui-react'

class Navigation extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <Container>
        <Grid>
          <GridRow/><Breadcrumbs page={this.props.page}/>
          <GridRow/>
        </Grid>
      </Container>
    )
  }
}

export default Navigation;
