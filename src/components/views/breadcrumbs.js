/**
 * Created by laugh on 7/14/2018.
 */
import React from 'react'
import { Icon, Step } from 'semantic-ui-react'

const Breadcrumbs = (props) => (
    <Step.Group fluid size='small'>
        {props.page === "home" &&
        <Step active>
            <Icon name='home' />
            <Step.Content>
                <Step.Title>Home</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "home" &&
        <Step disabled>
            <Icon name='gamepad'/>
            <Step.Content>
                <Step.Title>Questions</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "home" &&
        <Step disabled>
            <Icon name='trophy' />
            <Step.Content>
                <Step.Title>Results</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "questions" &&
        <Step>
            <Icon name='home' />
            <Step.Content>
                <Step.Title>Home</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "questions" &&
        <Step active>
            <Icon name='gamepad'/>
            <Step.Content>
                <Step.Title>Questions</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "questions" &&
        <Step disabled>
            <Icon name='trophy' />
            <Step.Content>
                <Step.Title>Results</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "results" &&
        <Step>
            <Icon name='home' />
            <Step.Content>
                <Step.Title>Home</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "results" &&
        <Step>
            <Icon name='gamepad'/>
            <Step.Content>
                <Step.Title>Questions</Step.Title>
            </Step.Content>
        </Step>
        }
        {props.page === "results" &&
        <Step active>
            <Icon name='trophy' />
            <Step.Content>
                <Step.Title>Results</Step.Title>
            </Step.Content>
        </Step>
        }
    </Step.Group>
);

export default Breadcrumbs