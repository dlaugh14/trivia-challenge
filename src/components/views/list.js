/**
 * Created by laugh on 7/15/2018.
 */
import React from 'react'
import { List} from 'semantic-ui-react'

const ListSection = (props) => (
    <div>
        <List>
            {props.questions && props.answers &&
                props.questions.map((item, index) => (

                    <List.Item textAlign="centered" key={"question" + index}>
                { props.answers[index].result === "correct" &&
                    <List.Icon name='check' color="green" />
                }
                { props.answers[index].result === "incorrect" &&
                    <List.Icon name='thumbs down' />
                }
                    <List.Content>
                        <span>{item.question}</span>
                            <span>{" - Correct answer: " + item.correct_answer +  " - Your answer: " + props.answers[index].answer}
                        </span>
                        </List.Content>
                    </List.Item>
                ))
            }
        </List>
    </div>
);

export default ListSection