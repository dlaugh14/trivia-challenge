/**
 * Created by laugh on 7/14/2018.
 */
import React from 'react'
import { Segment, Header } from 'semantic-ui-react'

const Title = (props) => (
    <Segment inverted>
        <Header as='h3' textAlign='center' color='teal'>
            Welcome to the Trivia Challenge!
        </Header>
    </Segment>
);

export default Title