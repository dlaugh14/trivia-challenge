/**
 * Created by laugh on 7/15/2018.
 */
import React from 'react'
import { Header } from 'semantic-ui-react'

const Description = (props) => (
    <div>
        <Header as={props.size} textAlign={props.alignment}>
            <Header.Content>{props.message}</Header.Content>
        </Header>
    </div>
);

export default Description