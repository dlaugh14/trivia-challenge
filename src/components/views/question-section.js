/**
 * Created by laugh on 7/15/2018.
 */
import React from 'react'
import { Card } from 'semantic-ui-react'

const QuestionSection = (props) => (
    <div>
        <Card.Group centered>
            <Card>
                <Card.Content
                    description={props.question}
                />
            </Card>
        </Card.Group>
    </div>
);

export default QuestionSection