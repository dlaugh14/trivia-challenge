/**
 * Created by laugh on 7/15/2018.
 */
import React from 'react'
import { Button } from 'semantic-ui-react'

const Buttons = (props) => (
    <div>
            <div className="ui one column stackable center aligned page grid">
                <div className="column twelve wide">
                    {console.log(props)}
                    {props.function &&
                    <Button value={props.text} onClick={props.function.bind(this)} color={props.color}>
                        {props.text}
                    </Button>
                    }
                    {!props.function &&
                    <Button color={props.color}>
                    {props.text}
                    </Button>
                    }
                </div>
            </div>
    </div>
);

export default Buttons