/**
 * Created by laugh on 9/21/2017.
 */
import {combineReducers} from 'redux';
import Questions from './trivia-questions';
import QuestionCount from './question-count';
import Answer from './answers';

/*
 * We combine all reducers into a single object before updated data is dispatched (sent) to store
 * Your entire applications state (store) is just whatever gets returned from all your reducers
 * */

const allReducers = combineReducers({
    questionCount: QuestionCount,
    results: Questions,
    answers: Answer
});

export default allReducers